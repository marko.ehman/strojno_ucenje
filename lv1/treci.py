numbers = []
while True:
    user_input = input("Please enter a number: ")
    if user_input == 'Done':
        break
    try:
        number = int(user_input)
    except:
        print("You did not enter a number!")
        continue
    numbers.append(number)

total_numbers = len(numbers)
average = sum(numbers)/total_numbers
max_value = max(numbers)
min_value = min(numbers)

print("Total numbers: ",total_numbers)
print("Average: ",average)
print("Max_value: ",max_value)
print("Min_value: ",min_value)

numbers.sort()
print(numbers)

for number in numbers:
    print(number)



